package com.example.gabrielpdezena.builddeployfinal;
import com.testfairy.TestFairy;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;



public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        TestFairy.begin(this, "e0d15a1f6c12a78f273a18444bf0ecc837267d32");
        setContentView(R.layout.activity_main);
        CrashManager.register(this, "4384baecd8d541b899482620eb1c0ab7");
        checkForUpdates();


    }

    @Override
    public void onResume() {
        super.onResume();
        // ... your own onResume implementation
        checkForCrashes();
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterManagers();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterManagers();
    }

    private void checkForCrashes() {
        CrashManager.register(this);
    }

    private void checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(this);
    }

    private void unregisterManagers() {
        UpdateManager.unregister();
    }



}
//TESTANDO COMMITdddkllkjlnmnnbmkjkbjkbkbdnmjhjhassadasmnkkjjjjjjkkjiiiuiuiunmmnjihuihuxs